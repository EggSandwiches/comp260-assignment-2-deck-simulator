import java.util.*;
import java.io.IOException;

public class CardShuffler {
	static int[] deck = {1,1,2,2,2,2,3,3,3,3,4,4,4,4,5,5,5,5,5,6,6,6,6,6,6,7,7,7,7,7,7,7,8,8,8,8,8,8,8,8};
	public static int numberInDeck = 40;
	
	public static int draw() {
		Random rand = new Random();
		int index = rand.nextInt(numberInDeck-1);
		int card = deck[index];
		remove(index);
		return card;
	}
	
	private static void remove(int i) {
		deck[i] = deck[numberInDeck-1];
		numberInDeck--;	
	}

	public static void main(String[] args) {
		boolean con = true;
		while(con){
			System.out.println("Draw a card?");
			Scanner s = new Scanner(System.in);
			int ans = s.nextInt();
			if(ans == 1) {
				int i = draw();
				if(i == 1) {
					System.out.println("Communism Strikes");
				} else if(i == 2){
					System.out.println("No you");
				} else if(i == 3) {
					System.out.println("Capitalism Strikes");
				} else if(i == 4) {
					System.out.println("Skip Turn");
				} else if(i == 5) {
					System.out.println("Negate");
				} else if(i == 6) {
					System.out.println("Ghando Strikes Again");
				} else if(i == 7) {
					System.out.println("Force Trade");
				} else if(i == 8) {
					System.out.println("Whats yours is mine");
				}
				System.out.println("cards left: " + numberInDeck);
			}
			if(numberInDeck == 0) {
				con = false;
			}
		}
	}
}
